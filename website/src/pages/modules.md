---
title: 模块与工具
---



import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';



## 模块（Modules）

### 接口文档生成器（Apidocs)

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-apidocs](https://github.com/suninformation/ymate-apidocs)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-apidocs](https://gitee.com/suninformation/ymate-apidocs)

</TabItem>
</Tabs>
:::

### 验证码（Captcha）

基于YMP框架实现的验证码模块，支持图片、邮件和短信三种验证类型，采用注解验证，配置简单、灵活，可自定义扩展；

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-module-captcha](https://github.com/suninformation/ymate-module-captcha)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-module-captcha](https://gitee.com/suninformation/ymate-module-captcha)

</TabItem>
</Tabs>
:::

### 嵌入式容器（Embed）

本项目为可执行嵌入式 Web 容器，在原始 WAR 包文件结构的基础上为其指定引导程序及相关依赖文件，并通过命令行方式直接启动 Web 服务，从而达到简化 Web 工程部署流程的目的。

[![Maven Central status](https://img.shields.io/maven-central/v/net.ymate.module/ymate-module-embed.svg)](https://search.maven.org/artifact/net.ymate.module/ymate-module-embed)

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-embed](https://github.com/suninformation/ymate-embed)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-embed](https://gitee.com/suninformation/ymate-embed)

</TabItem>
</Tabs>
:::

### 文件上传（Fileuploader）

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-module-fileuploader](https://github.com/suninformation/ymate-module-fileuploader)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-module-fileuploader](https://gitee.com/suninformation/ymate-module-fileuploader)

</TabItem>
</Tabs>

:::

### 开放授权（OAuth）

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-module-oauth](https://github.com/suninformation/ymate-module-oauth)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-module-oauth](https://gitee.com/suninformation/ymate-module-oauth)

</TabItem>
</Tabs>

:::

### 任务调度（Schedule）

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-module-schedule](https://github.com/suninformation/ymate-module-schedule)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-module-schedule](https://gitee.com/suninformation/ymate-module-schedule)

</TabItem>
</Tabs>

:::

### 单点登录（SSO）

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-module-sso](https://github.com/suninformation/ymate-module-sso)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-module-sso](https://gitee.com/suninformation/ymate-module-sso)

</TabItem>
</Tabs>

:::

### WEB 标签库（Taglib）

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-web-taglib](https://github.com/suninformation/ymate-web-taglib)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-web-taglib](https://gitee.com/suninformation/ymate-web-taglib)

</TabItem>
</Tabs>

:::

### 集成测试（Test）

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-platform-test](https://github.com/suninformation/ymate-platform-test)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-platform-test](https://gitee.com/suninformation/ymate-platform-test)

</TabItem>
</Tabs>

:::

### Unpack

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-module-unpack](https://github.com/suninformation/ymate-module-unpack)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-module-unpack](https://gitee.com/suninformation/ymate-module-unpack)

</TabItem>
</Tabs>

:::

### Websocket

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-module-websocket](https://github.com/suninformation/ymate-module-websocket)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-module-websocket](https://gitee.com/suninformation/ymate-module-websocket)

</TabItem>
</Tabs>

:::

## 原型（Archetypes）

### YMP 框架工程原型

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-maven-archetypes](https://github.com/suninformation/ymate-maven-archetypes)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-maven-archetypes](https://gitee.com/suninformation/ymate-maven-archetypes)

</TabItem>
</Tabs>

:::

### 管理后台工程原型

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-web-adminlte](https://github.com/suninformation/ymate-web-adminlte)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-web-adminlte](https://gitee.com/suninformation/ymate-web-adminlte)

</TabItem>
</Tabs>

:::

## 插件（Plugins）

### YMP 框架插件集

:::note 项目地址

<Tabs
defaultValue="github"
values={[
{ label: 'GitHub', value: 'github', },
{ label: '码云', value: 'gitee', },
]
}>
<TabItem value="github">

[https://github.com/suninformation/ymate-maven-plugin](https://github.com/suninformation/ymate-maven-plugin)

</TabItem>
<TabItem value="gitee">

[https://gitee.com/suninformation/ymate-maven-plugin](https://gitee.com/suninformation/ymate-maven-plugin)

</TabItem>
</Tabs>

:::


:::tip One More Thing

YMP 不仅提供便捷的 Web 及其它 Java 项目的快速开发体验，也将不断提供更多丰富的项目实践经验。

感兴趣的小伙伴儿们可以加入官方 QQ 群：[480374360](https://qm.qq.com/cgi-bin/qm/qr?k=3KSXbRoridGeFxTVA8HZzyhwU_btZQJ2)，一起交流学习，帮助 YMP 成长！

如果喜欢 YMP，希望得到你的支持和鼓励！

<Tabs
defaultValue="donationCode"
values={[
{label: '请喝一杯咖啡', value: 'donationCode'},
{label: '微信支付', value: 'wepay'},
{label: '支付宝', value: 'alipay'},
]}>
<TabItem value="donationCode"><img src="/img/donation_code.png" alt="donationCode"/></TabItem>
<TabItem value="wepay"><img src="/img/wepay.png" alt="WePay"/></TabItem>
<TabItem value="alipay"><img src="/img/alipay.jpeg" alt="AliPay"/></TabItem>
</Tabs>

了解更多有关 YMP 框架的内容，请访问官网：[https://ymate.net](https://ymate.net)

:::